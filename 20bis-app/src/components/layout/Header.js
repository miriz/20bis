import React from 'react';

import Title from './Title'
import SignIn from './SignIn';



const Header = (props) => {
  return (
    <div className="header">
      <Title />
      <SignIn {...props}/>
    </div>
  );
};




export default Header;