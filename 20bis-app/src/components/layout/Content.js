import React, { Component, useState, useEffect } from 'react';
import ResturantsPanel from '../resturants/ResturantsPanel';
import ApiService from '../../services/api-service'
const Content = (props) => {
  const [restaurants, setRestaurants] = useState([]);
  useEffect(() => {
    getRestaurants();
  }, [])

  function getRestaurants() {
   
    ApiService.post('getrestaurants', null).then((res) => {
      setRestaurants(res.data)
    });
  };




  return (
    <div className="container">

      <ResturantsPanel restaurants={restaurants} />

    </div>
  );
}
export default Content;
