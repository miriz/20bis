import React, { useState, useEffect, useReducer } from 'react';
import { withRouter } from 'react-router-dom';
import ApiService from '../../services/api-service'


const SignIn = (props) => {
    const [isSignIn, setIsSignIn] = useState(false);
    const [userDetails, setUserDetails] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            userName: '',
            password: '',
        }
    );
    const handleChange = evt => {
        const name = evt.target.name;
        const newValue = evt.target.value;
        setUserDetails({ [name]: newValue });
    }
    const signIn = () => {
        ApiService.post('signin/', { userDetails })
            .then((res) => {
                console.log('res.data', res.data);
                if (res.data) {
                    setIsSignIn(true);
                    props.history.push({
                        pathname: '/restaurantPage',
                        state: {
                            restaurant: res.data
                        }
                    })
                }
                else
                    alert("worng userdetails")
            });

    }

    const signOut = () => {
        setIsSignIn(false);
        props.history.push({
            pathname: '/',

        })

    }
    return (
        <div>
            {isSignIn
                ? <div>
                    <div className="sign-in-field" >hello to {userDetails.userName}</div>
                    <input className="btn btn-info" type="button" onClick={signOut} value="Sign out" />
                </div>
                :
                <div className="sign-in">
                    <input className="sign-in-field" name="userName" onChange={handleChange} placeholder="userName" />
                    <input className="sign-in-field" name="password" onChange={handleChange} type="password" placeholder="password" />
                    <input className="btn btn-info" type="button" onClick={signIn} value="Sign In" />
                </div>
            }
        </div>
    );
};




export default withRouter(SignIn);