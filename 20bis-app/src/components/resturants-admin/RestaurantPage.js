import React, { useState, useEffect, useReducer } from 'react';
import RestaurantMenu from '../resturants/RestaurantMenu'
import moment from 'moment';
import ApiService from '../../services/api-service';


const RestaurantPage = (props) => {
    console.log("props", props)
    let { restaurant } = props;

    if (!restaurant) {
        props.history.push({
            pathname: '/',
        })
    }
    const [editedRstaurant, setEditedRstaurant] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        JSON.parse(JSON.stringify(restaurant))
    );
    const [showEditDeliveryHours, setShowEditDeliveryHours] = useState(false);

    const editDeliveryHours = () => {
        setShowEditDeliveryHours(true);
    }
    const validateDeliveryHours = () => {
        if (!restaurant.deliveryHours.start || !restaurant.deliveryHours.end)
            return false;
        if (moment(restaurant.deliveryHours.start, 'HH:mm').isBefore(moment(restaurant.deliveryHours.end, 'HH:mm'))) {
            return true;
        }
        return false;


    }

    const saveDeliveryHours = () => {
        if (!validateDeliveryHours()) {
            alert('not valid time range');
            return;

        }
        ApiService.post('editdeliveryhours/', { restaurant: editedRstaurant })
            .then((res) => {
                setShowEditDeliveryHours(false);
                setEditedRstaurant(res.data)
            });
    }
    const undo = () => {
        setEditedRstaurant(restaurant)
        setShowEditDeliveryHours(false);
    }

    const handleChange = evt => {
        const name = evt.target.name;
        const newValue = evt.target.value;
        editedRstaurant.deliveryHours[name] = newValue;
        setEditedRstaurant(editedRstaurant);
    }

    return (
        <div>
            <div className="rest-info">
                <div className="name-rest">
                    <div className="rest-title">{restaurant.name}</div>
                    <div className="logo-div">
                        <img src="images/hero.jpg" className="logo" />
                    </div>

                    <div>
                        <div className="restaurant-address"><strong>
                                Address:
                                </strong>{restaurant.address}</div>
                        <div className="restaurant-address">
                            <strong>
                                Delivery hours:
                                </strong>
                            {(showEditDeliveryHours) ?
                                <div>
                                    <input value={editedRstaurant.deliveryHours.start} name="start" onChange={handleChange} />
                                    <input value={editedRstaurant.deliveryHours.end} name="end" onChange={handleChange} />
                                </div>
                                : ' ' + editedRstaurant.deliveryHours.start + '-' + editedRstaurant.deliveryHours.end
                            }
                        </div>
                        {(showEditDeliveryHours) ?
                            <React.Fragment>
                                <button className="btn btn-info" onClick={() => { saveDeliveryHours(); }}>save</button>
                                <button className="btn btn-info" onClick={() => { undo(); }}> undo</button>
                            </React.Fragment>
                            : <button className="btn btn-info" onClick={() => { editDeliveryHours(); }}> edit</button>}
                    </div>

                </div>

            </div>
            <RestaurantMenu menuId={restaurant.menuId} isAdmin={true} /></div>

    );
};




export default RestaurantPage;