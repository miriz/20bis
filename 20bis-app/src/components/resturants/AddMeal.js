
import React, { useState, useEffect, useReducer } from 'react';
import ApiService from '../../services/api-service';

const AddMeal = (props) => {
    let { menuId } = props;

    console.log('menuId/*/*-*/-*/-', menuId);

    const [newMeals, setNewMeals] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            name: '',
            description: '',
            price: 0.0,
        }
    );

    const handleChange = evt => {
        const name = evt.target.name;
        const newValue = evt.target.value;
        setNewMeals({ [name]: newValue });
    }

    const insertMeal = (newMeals) => {
        console.log('newMeals*/*/*/*/*/', newMeals);

        ApiService.post('insertmeal/', { newMeals: newMeals, menuId: menuId })
            .then((res) => {
                console.log('res.data', res.data);

                setNewMeals(res.data)
                props.onInsert(res.data);

            });
    };
    const undo = meal => {
        console.log(meal);
        props.onInsert(meal);


    }

    return (
        <div>
            <div>
                <div className="restaurant-address">
                    <strong>
                        Meal name:
                                </strong><input value={newMeals.name} name="name" onChange={handleChange} /></div>
                <div className="restaurant-address">
                    <strong>  Meal description: </strong>
                    <input value={newMeals.description} name="description" onChange={handleChange} /></div>

                <div className="restaurant-address"><strong>
                    Meal price:
                                </strong><input type="number" value={newMeals.price} name="price" onChange={handleChange} /></div>
                <button onClick={() => {
                    insertMeal(newMeals);
                }}> save</button>
                <button onClick={() => {
                    undo(newMeals);
                }}> undo</button>

            </div>
        </div>




    );
};



export default AddMeal;