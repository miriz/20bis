import React from 'react';
import { Switch, Route, Link } from 'react-router-dom';
import ListAllRestaurants from './ListRestaurant';
import RestaurantMenu from './RestaurantMenu';
import Title from '../layout/Title';
import RestaurantPage from '../resturants-admin/RestaurantPage';

const ResturantsPanel = (props) => {
  let { restaurants } = props

  return (
    <div className="all-restaurants">
      <Switch>
        <Route exact path="/" render={(props) => (
          <ListAllRestaurants {...props} restaurants={restaurants} />
        )}>
        </Route>
        <Route exact path="/showmenu/:menuId" render={(props) => (
          <RestaurantMenu  menuId={props.match.params["menuId"]} />
        )}></Route>
        <Route exact path="/restaurantPage" render={(props) => (
          <RestaurantPage {...props.location.state} />
        )}></Route>
      </Switch>
    </div>
  );
};



export default ResturantsPanel;