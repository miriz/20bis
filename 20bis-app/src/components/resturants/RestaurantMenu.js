import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Meal from './Meal';
import Order from './Orders';
import ApiService from '../../services/api-service';

const RestaurantMenu = ({ menuId, isAdmin }) => {
    const [meals, setMeals] = useState([]);
    const [orderMeals, setOrderMeals] = useState([]);
    const [showAddMeal, setShowAddMeal] = useState(false);
    const [isEdit, setIsEdit] = useState(false);


    useEffect(() => {
        getMenu();
    }, [])

    const getMenu = () => {
        ApiService.post('getmenu/' + menuId, null)
            .then((res) => {
                console.log('res.data', res.data);
                setMeals(res.data)
            });
    };
    const deleteMeal = (meal) => {
        let id = meal._id;
        ApiService.post('deletemeal' + id, { menuId: menuId })
            .then((res) => {
                getMenu();
                //setMeals(res.data)
            });
    }
    const editMeal = (meal) => {
        setIsEdit(true);

    }
    const handleOrderMeal = (meal) => {
        setOrderMeals([...orderMeals, meal]);
        console.log('orderMeals', orderMeals);
    }
    const addMeal = () => {
        setShowAddMeal(true);
    }
    const handleInsert = () => {
        setShowAddMeal(false);
        getMenu();
    }
    const handleEdit = () => {
        getMenu();
    }



    const handleDeleteOrder = (meals) => {
        console.log("meals", meals);

        setOrderMeals(meals.map(e => e))

    }
    return (
        <div>

            <div className={(isAdmin) ? '' : 'meals-info'}>


                <div className="menu-title">Menu</div>
                {
                    (isAdmin && !showAddMeal) ?
                        <button className="btn btn-info" onClick={() => { addMeal(); }}> add</button> :
                        null}
                {(showAddMeal) ? <div className="meal-info-admin"><Meal isAdd={showAddMeal} menuId={menuId} isAdmin={isAdmin} onInsert={handleInsert} /> </div> : null}

                {
                    meals.map((meal) => {
                        return (
                            <div key={meal._id} className={(isAdmin) ? 'meal-info-admin' : 'meal-info'}>
                                <div>
                                    <Meal isAdmin={isAdmin} menuId={menuId} meal={meal} onEdit={handleEdit} onOrder={handleOrderMeal} />


                                </div>
                            </div>
                        )
                    })
                }
            </div>
            {(isAdmin) ? null : (<div>
                <div>
                    <Order onDeleteOrder={handleDeleteOrder} orderMeals={orderMeals} />
                </div>
                <Link to={
                    {
                        pathname: `/`
                    }
                }><input className="btn-back btn btn-info" type="button" value="Back" /></Link>
            </div>
            )}
        </div>
    );
};



export default RestaurantMenu;