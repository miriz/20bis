import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import axios from 'axios';
import { log } from 'util';
import AddMeal from './AddMeal';

const Order = (props) => {
    let { orderMeals } = props;
    const [sum, setSum] = useState([]);

    useEffect(() => {
        let prices = orderMeals.map(e => e.price)
        let sum1 = prices.reduce((total, num) =>
            parseFloat(total) + parseFloat(num), 0
        );
        setSum(sum1);
    }, [orderMeals])

    const deleteMeal = (meal) => {
        let mealIndex = props.orderMeals.findIndex(e => { return e._id == meal._id });
        props.orderMeals.splice(mealIndex, 1)

        props.onDeleteOrder(props.orderMeals);
    }

    const saveOrder = () => {
        //go to server and save order
    }


    return (
        <div>
            <div className="menu-title">Order</div>
            <div  className="order-info">
                {
                    props.orderMeals.map((meal) => {
                        return (
                            <div key={meal._id}>
                                <div>
                                    <div className="restaurant-address">
                                        <strong>
                                            Meal name:
                                </strong>{meal.name}</div>
                                    <div className="restaurant-address">
                                        <strong>
                                            Meal description:
                                </strong>
                                        {' ' + meal.description}</div>

                                    <div className="restaurant-address"><strong>
                                        Meal price:
                                </strong>{meal.price}</div>

                                    <button className="btn btn-info" onClick={() => {
                                        deleteMeal(meal);
                                    }}> delete</button>


                                </div>
                            </div>
                        )
                    })
                }
                <div>total - {sum}</div>
            </div></div>
    );
};



export default Order;