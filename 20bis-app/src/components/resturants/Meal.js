
import React, { useState, useEffect, useReducer } from 'react';
import ApiService from '../../services/api-service';
const Meal = (props) => {
    let { meal, menuId, isAdmin, isAdd } = props;
    const [isEdit, setIsEdit] = useState(isAdd || false);

    const [editedMeal, setMeal] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        meal ? meal : {
            name: '',
            description: '',
            price: 0
        }
    );

    const handleChange = evt => {
        const name = evt.target.name;
        const newValue = evt.target.value;
        setMeal({ [name]: newValue });
    }
    const editMeal = (meal) => {
        setIsEdit(true);

    }
    const isValid = (meal) => {
        let requiredProps = Object.keys(meal);
        return requiredProps.some((key) => {
            return meal[key] === null || meal[key] === ''
        })
    }
    const saveMeal = () => {
        let isNotValid = isValid(editedMeal);

        if (isNotValid) {
            alert('Not valid')
            return;
        }
        if (editedMeal._id)
            ApiService.post('editmeal', { meal: editedMeal, menuId: menuId })
                .then((res) => {
                    setMeal(res.data)
                    props.onEdit(res.data);
                    setIsEdit(false);

                });
        else
            ApiService.post('insertmeal', { meal: editedMeal, menuId: menuId })
                .then((res) => {
                    setMeal(res.data)
                    props.onInsert();
                });
    };
    const deleteMeal = (meal) => {
        let id = meal._id;
        ApiService.post('deletemeal/' + id, { menuId: menuId })
            .then((res) => {
                props.onEdit();
                //setMeals(res.data)
            });
    }
    const undo = () => {
        if (editedMeal._id) {
            setMeal(meal);
            setIsEdit(false)
            props.onEdit();
        }
        else
            props.onInsert();
    }
    const order = meal => {
        props.onOrder(meal);
    }

    return (
        <div>
            {isEdit ?
                <div>
                    <div className="restaurant-address">
                        <strong>
                            Meal name:
                                </strong><input value={editedMeal.name} name="name" onChange={handleChange} /></div>
                    <div className="restaurant-address">
                        <strong>  Meal description: </strong>
                        <input value={editedMeal.description} name="description" onChange={handleChange} /></div>

                    <div className="restaurant-address"><strong>
                        Meal price:
                                </strong><input type="number" value={editedMeal.price} name="price" onChange={handleChange} /></div>
                    <button className="btn btn-info" onClick={() => {
                        saveMeal();
                    }}> save</button>
                    <button className="btn btn-info" onClick={() => {
                        undo();
                    }}> undo</button>

                </div>
                :
                <div>
                    <div>
                        <div className="restaurant-address">
                            <strong>
                                Meal name:
                                </strong>{editedMeal.name}</div>
                        <div className="restaurant-address">
                            <strong>
                                Meal description:
                                </strong>
                            {' ' + editedMeal.description}</div>

                        <div className="restaurant-address"><strong>
                            Meal price:
                                </strong>{editedMeal.price}
                        </div>
                    </div>
                    {(isAdmin) ? (
                        <React.Fragment>
                            <button className="btn btn-info" onClick={() => {
                                editMeal(editedMeal);
                            }}> edit</button>
                            <button  className="btn btn-info" onClick={() => {
                                deleteMeal(editedMeal);
                            }}> delete</button>
                        </React.Fragment>
                    ) : (
                            <button className="btn btn-info" onClick={() => {
                                order(editedMeal);
                            }}> add to order</button>)
                    }
                </div>
            }

        </div>





    );
};



export default Meal;