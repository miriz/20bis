import React, { Component, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';

import axios from 'axios';



const ListAllRestaurants = (props) => {
    let { restaurants } = props

    const checkDeliveryHours = (restaurant) => {
        let time = moment(new Date(), 'HH:mm');
        if (time.isAfter(moment(restaurant.deliveryHours.start, 'HH:mm')) && time.isBefore(moment(restaurant.deliveryHours.end, 'HH:mm'))) {
            return true;
        }
        return false;
    }
    return (
        restaurants.map((restaurant) => {
            return (

                <div className="row">
                    <div className="col-sm-2"></div>
                    <div className="col-sm-8">
                        <div key={restaurant._id} className="restaurant-info">
                            <div className="name-address">
                                <div className="logo-div">
                                    <img src="images/hero.jpg" className="logo" />
                                </div>
                                <div>
                                    <div className="restaurant-address">{restaurant.name}</div>

                                    <div className="restaurant-address">{restaurant.address}</div>
                                    <div className="restaurant-address">
                                        <strong>
                                            Delivery hours:
                                </strong>
                                        {' ' + restaurant.deliveryHours.start + '-' + restaurant.deliveryHours.end}</div>

                                    <div className="restaurant-rating">{restaurant.description}</div>
                                </div>
                            </div>
                            {checkDeliveryHours(restaurant) ? <Link to={
                                {
                                    pathname: `showmenu/${restaurant.menuId}`,
                                    restaurant
                                }
                            }>< button className="btn btn-info">order...</button>
                            </Link> :
                                <div>  < button className="btn btn-info" disabled>closed</ button></div>}
                        </div ></div></div>
            )
        })
    );
}


export default ListAllRestaurants