import axios from "axios";
const ApiService = {
    baseUrl: "https://localhost:3001/services/",

    post: function (url, params) {
        return axios.post('http://localhost:3001/services/' + url, params)
    },

    get: function (url) {
        return axios.get('http://localhost:3001/services/' + url)
    }
};

export default ApiService;