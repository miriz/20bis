import React, { Component } from 'react';
import Header from './components/layout/Header';
import Content from './components/layout/Content';
import { BrowserRouter } from 'react-router-dom';

import './css/style.css'

const App = (props) => {
  return (
    <BrowserRouter>
      <div >
        <Header />
        <Content />
      </div>
    </BrowserRouter>

  );
}
export default App
