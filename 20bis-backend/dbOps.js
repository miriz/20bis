const mealsOps = require('./collectionOps/mealsOps')
const restaurantsOps = require('./collectionOps/restaurantOps')
const userOps = require('./collectionOps/userOps')

// dependencies
const mongodb = require('mongodb')
const MongoClient = mongodb.MongoClient;
const ObjectId = mongodb.ObjectID

const URI_TO_CONNECT_MONGODB = "mongodb+srv://admin:1234@20bis-vulz6.mongodb.net/test?retryWrites=true&w=majority";
const DB_NAME = "test"
const COLLECTION_MENUS = "menus"
const COLLECTION_MEALS = "meals"
// this function will connect db and based on API send response
let connectDbAndRunQueries = async (apiName, req, res) => {
	try {
		let client = await MongoClient.connect(URI_TO_CONNECT_MONGODB)
		// select the db, Collections are selected based on needs
		const db = client.db(DB_NAME)

		// default output
		const output = { "message": "SUCCESS" }

		// perform several db actions based on API names
		await chooseApiAndSendResponse(apiName, db, req, res, client, output)
	} catch (err) {
		console.log('Some Error occurred ...', err)
	}
}


// choose the particular function for an API and process it
let chooseApiAndSendResponse = async (apiName, db, req, res, client, output) => {

	try	{// perform db specific ops based on API names
	switch (apiName) {
		case 'signIn':
			output = await userOps.makeSignIn(db, req, res, client, output)
			break;
		case 'getRestaurants':
			output = await restaurantsOps.makeGetRestaurants(db, req, res, client, output)
			break;
		case 'getMenu':
			output = await mealsOps.makeGetMenu(db, req, res, client, output)
			break;
		case 'saveOrder':
			makeSaveOrder(db, req, res, client, output)
			break;
		case 'deleteMeal':
			console.log('makedeletemeal');
			output = await mealsOps.makeDeleteMeal(db, req, res, client, output)
			break;
		case 'editMeal':
			output = await mealsOps.makeEditMeal(db, req, res, client, output)
			break;
		case 'insertMeal':
			console.log('makeinsertmeal');
			output = await mealsOps.makeInsertMeal(db, req, res, client, output)
			break;
		case 'editDeliveryHours':
			output = await restaurantsOps.makeSaveDeliveryHours(db, req, res, client, output)
			break;

	}
	console.log('OUTPUT2', output);

	sendOutputAndCloseConnection(client, output, res)
}
	catch(err){
		sendOutputAndCloseConnection(client, output, res)
	}
}
// send the response and close the db connection
function sendOutputAndCloseConnection(client, output, res) {
	if (res) {
		console.log(`========================\nOUTPUT AS RECEIVED AND BEFORE SENDING\n==================\n`, output)
		res.json(output)
	}


	// close the database connection after sending the response
	client.close()
}

// exports
module.exports = {
	connectDbAndRunQueries
}