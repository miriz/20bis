// imports dependencies
const express = require('express')
const cors = require('cors')

const morgan = require('morgan')
const bodyParser = require('body-parser')


// local file dependencies
const dbOps = require('./dbOps')
let router = express.Router()


// middlewares
router.use(cors())
router.use(bodyParser.json({ type: 'application/json' }))
router.use(morgan('dev'))

// most important to serve static pages don't forget
// router.use(express.static('../public'))
router.post('/signin', (req, res) => {
	dbOps.connectDbAndRunQueries('signIn', req, res)

})
router.post('/getrestaurants/:item?', (req, res) => {
	dbOps.connectDbAndRunQueries('getRestaurants', req, res)

})
router.post('/getmenu/:menuId?', (req, res) => {
	dbOps.connectDbAndRunQueries('getMenu', req, res)

})
router.post('/deletemeal/:id?', (req, res) => {
	dbOps.connectDbAndRunQueries('deleteMeal', req, res)

})
router.post('/editmeal/', (req, res) => {
	dbOps.connectDbAndRunQueries('editMeal', req, res)

})
router.post('/insertmeal', (req, res) => {
	dbOps.connectDbAndRunQueries('insertMeal', req, res)
})
router.post('/editdeliveryhours', (req, res) => {
	dbOps.connectDbAndRunQueries('editDeliveryHours', req, res)
})



module.exports = router