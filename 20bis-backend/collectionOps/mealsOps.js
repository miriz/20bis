const COLLECTION_MEALS = "meals"
const COLLECTION_MENUS = "menus"
const mongodb = require('mongodb')

const ObjectId = mongodb.ObjectID


let makeDeleteMeal = async (db, req, res, client, output) => {
    let { id } = req.params
    let { menuId } = req.body
    console.log('MENUID -**/+998+8+8+6', menuId);

    try {
        let data = await db
            .collection(COLLECTION_MEALS)
            .deleteOne({ "_id": ObjectId(id) })
        let bla = await db
            .collection(COLLECTION_MENUS)
            .update(
                { _id: ObjectId(menuId) },
                { $pull: { meals: ObjectId(id) } }
            )
        return bla
    } catch (error) {
        console.log('unable to get all the users', error)
        throw(error)
    }
}
let makeEditMeal = async (db, req, res, client, output) => {
    let { meal } = req.body
    try {
        let id = meal._id;
        delete meal._id;
        console.log('meal ID-*//-**/-**/-**/-*/', id);

        console.log('meal-*//-**/-**/-**/-*/', meal);

        let data = await db
            .collection(COLLECTION_MEALS)
            .updateOne(
                { _id: ObjectId(id) },
                { $set: meal }
            )
        return data

    } catch (error) {
        console.log('unable to get all the users', error)
        throw(error)
    }
}
let makeInsertMeal = async (db, req, res, client, output) => {
    let { meal, menuId } = req.body
    try {
        // db call 
        let data = await db
            .collection(COLLECTION_MEALS)
            .insert(meal)
        let newId = data.insertedIds[0];
        let menuData = await db
            .collection(COLLECTION_MENUS)
            .update(
                { _id: ObjectId(menuId) },
                { $push: { meals: newId } }
            )
        return menuData

    } catch (error) {
        console.log('unable to get all the users', error)
        throw(error)
    }
}
let makeGetMenu = async (db, req, res, client, output) => {
    let { menuId } = req.params
    try {
        let data = await db
            .collection(COLLECTION_MENUS)
            .findOne({ "_id": ObjectId(menuId) })
        let meals = await db.collection(COLLECTION_MEALS)
            .find({ _id: { $in: data.meals } })
            .toArray()
        return meals;
    } catch (error) {
        console.log('unable to get all the users', error)
        throw(error)
    }
}

// exports
module.exports = {
    makeEditMeal,
    makeDeleteMeal,
    makeInsertMeal,
    makeGetMenu
}