const COLLECTION_USERS = "users"
const COLLECTION_RESTAURANTS = "resturants"

const mongodb = require('mongodb')

const ObjectId = mongodb.ObjectID



let makeSignIn = async (db, req, res, client, output) => {
    let { userDetails } = req.body
    console.log("userDetails", userDetails)
    try {
        let userData = await db
            .collection(COLLECTION_USERS)
            .findOne({ "userName": userDetails.userName, "password": userDetails.password })
        console.log("userData", userData);

        let data = null;
        if (userData) {
            data = await db
                .collection(COLLECTION_RESTAURANTS)
                .findOne({ "_id": ObjectId(userData.restaurantId) })
        }

        return data

    } catch (error) {
        console.log('unable to get all the users', error)
        throw(error)
    }
}
// exports
module.exports = {
    makeSignIn
}