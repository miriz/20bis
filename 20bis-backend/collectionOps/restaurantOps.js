const COLLECTION_RESTAURANTS = "resturants"
const mongodb = require('mongodb')

const ObjectId = mongodb.ObjectID


let makeGetRestaurants = async (db, req, res, client, output) => {
    try {
        let data = await db
            .collection(COLLECTION_RESTAURANTS)
            .find()
            .toArray()
        console.log('dasdasdasd', data);

        return data;
    } catch (error) {
        console.log('unable to get all the users', error)
        throw(error)
    }
}


let makeSaveDeliveryHours = async (db, req, res, client, output) => {
    let { restaurant } = req.body
    try {
        let data = await db
            .collection(COLLECTION_RESTAURANTS)
            .updateOne(
                { _id: ObjectId(restaurant._id) },
                { $set: { deliveryHours: restaurant.deliveryHours } }
            )
        return data;
    } catch (error) {
        console.log('unable to get all the users', error)
        throw(error)
    }
}
// exports
module.exports = {
    makeGetRestaurants,
    makeSaveDeliveryHours
}