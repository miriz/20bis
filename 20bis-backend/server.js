// imports dependencies
let express = require('express')
let app = express()
const morgan = require('morgan')

// router
const route = require('./routes')

// PORT 3001
const PORT = 3001

// serve the static pages
app.use(morgan("dev"))
app.use(express.static('../dist'))


app.use('/services', route)
app.listen(PORT, () => {
	console.log('Server is running on ', PORT)
})